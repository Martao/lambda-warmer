﻿using Amazon.Lambda;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace LambdaWarmer
{
    public class LambdaWarmer
    {
        /// <summary>
        /// The method will keep the lambda warm and return true if the current call is a warm up.
        /// </summary>
        /// <param name="request"></param>
        public async Task<bool> Warmer(APIGatewayProxyRequest request)
        {
            var funcName = Environment.GetEnvironmentVariable("AWS_LAMBDA_FUNCTION_NAME");

            if (request.Resource == "WarmingLambda")
            {
                LambdaLogger.Log($"Warming Lambda { funcName }");

                int.TryParse(request.Body, out var concurrencyCount);

                if (concurrencyCount > 1)
                {
                    LambdaLogger.Log($"Warming instance {concurrencyCount}.");
                    var client = new AmazonLambdaClient();
                    await client.InvokeAsync(new Amazon.Lambda.Model.InvokeRequest
                    {
                        FunctionName = funcName,
                        InvocationType = InvocationType.RequestResponse,
                        Payload = JsonConvert.SerializeObject(new APIGatewayProxyRequest
                        {
                            Resource = request.Resource,
                            Body = (concurrencyCount - 1).ToString()
                        })
                    });
                }
                return true;
            }
            return false;
        }
    }
}
